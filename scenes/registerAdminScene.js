import { Scenes } from "telegraf";
import { User } from "../sequelize.js";
import { Responses, assignResponse } from "./responses.js";

const { WizardScene } = Scenes;

const registerAdminScene = {
    id: `REGISTER_ADMIN`,
    command: `/registeradmin`,
    regex: /^\/registeradmin$/,
    description: `Register admin baru`,
    scene: undefined,
};

registerAdminScene.scene = new WizardScene(
    registerAdminScene.id,

    // #1 START
    async (ctx) => {
        ctx.session.nAdminData = {};
        await ctx.replyWithHTML(
            `Masukkan <b>nama admin baru</b>\n\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #2 INPUT NAMA
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input admin baru dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (answer.length <= 1) {
            await ctx.replyWithHTML(`input nama <b>terlalu pendek</b>`);
            await ctx.replyWithHTML(`Masukan <b>nama admin baru</b>`);
            return;
        }

        ctx.session.nAdminData.nama = answer;
        await ctx.replyWithHTML(`Input <b>Telegram Username</b> admin`);
        return ctx.wizard.next();
    },

    // #3 TELEGRAM USERNAME
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input admin baru dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (answer.length <= 1) {
            await ctx.replyWithHTML(
                `input telegram username <b>terlalu pendek</b>`
            );
            await ctx.replyWithHTML(`Input <b>Telegram Username</b> admin`);
            return;
        }

        ctx.session.nAdminData.telegramUsername = answer;

        let nAdmin = ctx.session.nAdminData;
        await ctx.replyWithHTML(
            `Admin Baru:\n\nNama : <b>${nAdmin.nama}</b>\nTelegram Username : <b>${nAdmin.telegramUsername}</b>\n\n<b>/confirm</b> untuk melanjutkan\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #4 CONFIRMATION
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input admin baru dibatalkan`);
                return ctx.scene.leave();
            case Responses.confirmCommand:
                let nAdminData = ctx.session.nAdminData;

                // CREATE USER
                let nUser = await User.create({
                    nama: nAdminData.nama,
                    telegramUsername: nAdminData.telegramUsername,
                });
                // CREATE ADMIN
                await nUser.createAdmin({
                    nama: nAdminData.nama,
                });

                await ctx.replyWithHTML(`Admin baru telah dibuat`);
                return ctx.scene.leave();
            default:
                return;
        }
    }
);

export { registerAdminScene };

export default registerAdminScene;
