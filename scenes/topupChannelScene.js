import { Scenes } from "telegraf";
import { AgentActionLog } from "../sequelize.js";
import { Responses, assignResponse } from "./responses.js";

const { WizardScene } = Scenes;

const topupChannelScene = {
    id: `TOPUP_CHANNEL`,
    command: `/topup`,
    regex: /^\/topup$/,
    description: `Topup pulsa melalui channel2channel`,
    scene: undefined,
};

topupChannelScene.scene = new WizardScene(
    topupChannelScene.id,

    // #1 START
    async (ctx) => {
        ctx.session.nominalTopup = null;
        await ctx.replyWithHTML(
            `Masukkan <b>Nominal</b>\n\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #2 NOMINAL INPUT
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Topup channel dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (!/^\d+$/.test(answer)) {
            await ctx.replyWithHTML(`Nominal <b>Harus angka</b>`);
            await ctx.replyWithHTML(`Masukkan <b>Nominal</b>`);
            return;
        }

        let nominal = parseInt(answer);
        ctx.session.nominalTopup = nominal;
        let cAgent = ctx.agent;
        await ctx.replyWithHTML(
            `Topup ke channel\n\nNomor Channel : <b>${cAgent.nomorChannel}</b>\nNominal : <b>${nominal}</b>\n\n<b>/confirm</b> untuk melanjutkan\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #3 CONFIRM
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Topup channel dibatalkan`);
                return ctx.scene.leave();
            case Responses.confirmCommand:
                // [Progress]
                // puppeteerbot does something
                await ctx.replyWithHTML(`Mohon ditunggu`);
                return ctx.scene.leave();
            default:
                return;
        }
    }
);

export { topupChannelScene };

export default topupChannelScene;
