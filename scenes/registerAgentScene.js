import { Scenes } from "telegraf";
import { User } from "../sequelize.js";
import { Responses, assignResponse } from "./responses.js";

const { WizardScene } = Scenes;

const registerAgentScene = {
    id: `REGISTER_AGENT`,
    command: `/registeragent`,
    regex: /^\/registeragent$/,
    description: `Register agent baru`,
    scene: undefined,
};

registerAgentScene.scene = new WizardScene(
    registerAgentScene.id,

    // #1 START
    async (ctx) => {
        ctx.session.nAgentData = {};
        await ctx.replyWithHTML(
            `Masukkan <b>nama agent baru</b>\n\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #2 INPUT NAMA
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input agent baru dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (answer.length <= 1) {
            await ctx.replyWithHTML(`input nama <b>terlalu pendek</b>`);
            await ctx.replyWithHTML(`Masukan <b>nama agent baru</b>`);
            return;
        }

        ctx.session.nAgentData.nama = answer;
        await ctx.replyWithHTML(`Input <b>nomor channel</b> agent`);
        return ctx.wizard.next();
    },

    // #3 NOMOR CHANNEL
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input agent baru dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (!/^\d{9,}$/.test(answer)) {
            await ctx.replyWithHTML(`Input nomor channel <b>tidak valid</b>`);
            await ctx.replyWithHTML(`Input <b>nomor channel</b> agent`);
            return;
        }

        ctx.session.nAgentData.nomorChannel = answer;
        await ctx.replyWithHTML(`Input <b>Telegram Username</b> agent`);
        return ctx.wizard.next();
    },

    // #4 TELEGRAM USERNAME
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input agent baru dibatalkan`);
                return ctx.scene.leave();
        }

        // VALIDATE INPUT
        if (answer.length <= 1) {
            await ctx.replyWithHTML(
                `input telegram username <b>terlalu pendek</b>`
            );
            await ctx.replyWithHTML(`Input <b>Telegram Username</b> agent`);
            return;
        }

        ctx.session.nAgentData.telegramUsername = answer;
        await ctx.replyWithHTML(
            `Input <b>keterangan</b> agent <i>(boleh kosong)</i>`
        );
        return ctx.wizard.next();
    },

    // #5 KETERANGAN
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input agent baru dibatalkan`);
                return ctx.scene.leave();
        }

        ctx.session.nAgentData.keterangan = answer;
        let nAgent = ctx.session.nAgentData;
        await ctx.replyWithHTML(
            `Agent Baru:\n\nNama : <b>${nAgent.nama}</b>\nNomor Channel : <b>${nAgent.nomorChannel}</b>\nTelegram Username : <b>${nAgent.telegramUsername}</b>\nKeterangan : <b>${nAgent.keterangan}</b>\n\n<b>/confirm</b> untuk melanjutkan\n<b>/leave</b> untuk batal`
        );
        return ctx.wizard.next();
    },

    // #6 CONFIRMATION
    async (ctx) => {
        let answer = ctx.message.text;

        // ASSIGNED COMMAND CHECKER
        switch (assignResponse(answer)) {
            case Responses.leaveCommand:
                await ctx.replyWithHTML(`Input agent baru dibatalkan`);
                return ctx.scene.leave();
            case Responses.confirmCommand:
                let nAgentData = ctx.session.nAgentData;

                // CREATE USER
                let nUser = await User.create({
                    nama: nAgentData.nama,
                    telegramUsername: nAgentData.telegramUsername,
                });
                // CREATE AGENT
                await nUser.createAgent({
                    nama: nAgentData.nama,
                    nomorChannel: nAgentData.nomorChannel,
                    keterangan: nAgentData.keterangan,
                });

                await ctx.replyWithHTML(`Agent baru telah dibuat`);
                return ctx.scene.leave();
            default:
                return;
        }
    }
);

export { registerAgentScene };

export default registerAgentScene;
