export const Responses = {
    leaveCommand: 100,
    confirmCommand: 200,
    skipCommand: 300,
    none: 1000,
};

export const assignResponse = (text) => {
    if (/^\/leave/.test(text)) return Responses.leaveCommand;
    if (/^\/confirm/.test(text)) return Responses.confirmCommand;
    if (/^\/skip/.test(text)) return Responses.skipCommand;
    return Responses.none;
};
