import "dotenv/config";
import { sequelize, AppCredential, User } from "./sequelize.js";

//
// SEEDING DATABASE
//

// SEEDING
// CREDENTIALS
//
sequelize.sync({ alter: true }).then(async () => {
    let appCred1 = await AppCredential.create({
        workerId: 1,
        username: `08988087781`,
        password: `Apd@prb2`,
        pin: `1973`,
    });
    await AppCredential.create({
        workerId: 2,
        username: `0895371051997`,
        password: `Apd@jbr3`,
        pin: `1234`,
    });
    let superadmin = await User.create({
        nama: `Superadmin`,
        telegramId: 685825640,
        telegramUsername: `frozt86`,
    });
    await superadmin.createAdmin({
        nama: superadmin.nama,
        isSuperadmin: true,
    });
    let superagent = await superadmin.createAgent({
        nama: `Superagent`,
        nomorChannel: `08970606197`,
        keterangan: `Superadmin`,
    });

    await appCred1.addAgent(superagent);
});
