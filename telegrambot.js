import { Telegraf, Scenes, session } from "telegraf";
import { Agent, User, Admin } from "./sequelize.js";
import { topupChannelScene } from "./scenes/topupChannelScene.js";
import { registerAgentScene } from "./scenes/registerAgentScene.js";
import { registerAdminScene } from "./scenes/registerAdminScene.js";

const { Stage } = Scenes;

export const bot = new Telegraf(process.env.TELEGRAM_BOT_TOKEN);

// *****************
// CONSTANT INSTANCE
// *****************
//
// SUPERADMIN
//
let superadminChatId;
(async () => {
    let superadmin = await Admin.findOne({ where: { isSuperadmin: true } });
    superadminChatId = (await superadmin.getUser()).telegramId;
})();

// **********
// MIDDLEWARE
// **********

//
// USER DB CHECK MIDDLEWARE
//
bot.use(async (ctx, next) => {
    let cUser = await User.findOne({
        where: { telegramUsername: ctx.message.chat.username },
        include: [Admin, Agent],
    });
    if (!cUser) {
        return;
    }
    if (!cUser.telegramId) {
        cUser.telegramId = ctx.message.chat.id;
        await cUser.save();
    }
    ctx.user = cUser;
    ctx.admin = ctx.user.admin;
    ctx.agent = ctx.user.agent;
    return next();
});

//
// SCENES ATTACH
//
bot.use(session());
const stage = new Stage([
    topupChannelScene.scene,
    registerAgentScene.scene,
    registerAdminScene.scene,
]);
bot.use(stage.middleware());

//
// COMMANDS HANDLER
//
bot.command(`menu`, async (ctx) => {
    let menu = ``;
    if (ctx.admin) menu += ``;
    if (ctx.agent) menu += ``;
});
bot.on(`message`, async (ctx) => {
    if (registerAgentScene.regex.test(ctx.message.text)) {
        if (!ctx.admin) return;
        return ctx.scene.enter(registerAgentScene.id);
    }
    if (registerAdminScene.regex.test(ctx.message.text)) {
        if (!ctx.admin.isSuperadmin) return;
        return ctx.scene.enter(registerAdminScene.id);
    }
    if (topupChannelScene.regex.test(ctx.message.text)) {
        if (!ctx.agent) return;
        return ctx.scene.enter(topupChannelScene.id);
    }
    if (/^\/tembak\s\d+$/.test(ctx.message.text)) {
        if (!ctx.agent) return;
        ctx.replyWithHTML(`under construction`);
    }
});

//
// ERROR HANDLER
//
bot.catch((err, ctx) => {
    console.log(`ERROR HAPPEN`);
    console.error(err);
});

export const startTelegramBot = async () => {
    try {
        await bot.launch();
    } catch (err) {
        console.log(`BOT ERROR`);
        console.error(err);
        startTelegramBot();
    }
};
