import "dotenv/config";
// import { apiFoo } from "./devtest2.js";

// setTimeout(async () => {
//     apiFoo(`satu satu aku sayang ibu`).then((val) => console.log(val));
//     apiFoo(`dua dua aku sayang ayah`).then((val) => console.log(val));
//     apiFoo(`tiga tiga sayang sayang adik kakak`).then((val) =>
//         console.log(val)
//     );
//     apiFoo(`lihat kebunku, penuh dengan bunga`).then((val) => console.log(val));
// }, 1000);

// import { fork } from "node:child_process";

// fork(`./puppetService.js`, [`satu`, `dua`, `tiga`]);
// fork(`./puppetService.js`, [`empat`, `lima`, `enam`]);

import { Agent, User } from "./sequelize.js";

Agent.findOne({
    where: { id: 2 },
    include: [User],
}).then((tAgent) => {
    let oAgent = tAgent.toJSON();
    delete oAgent.user;
    console.log(oAgent);
});
