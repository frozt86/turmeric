import "dotenv/config";
import cluster from "node:cluster";
import process from "node:process";
import { Agent, User, Admin, AppCredential } from "./sequelize.js";
import { Telegram } from "telegraf";

import puppeteer, { TimeoutError } from "puppeteer";

const puppetBots = [];

// *********
// BOT ERROR
// *********
class BotError extends Error {
    constructor(message, errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}

const BOT_ERROR_CODES = {
    loginFailed: 5001,
    procedureStepFailed: 5002,
    webpageChanged: 5003,
    navigationFailed: 5004,
    dataNotAvailable: 5005,
};

// RESOLVER ARRAY
const resolvers = [];

// ******************
// * MASTER PROCESS *
// ******************
if (cluster.isPrimary) {
    (async () => {
        let appCreds = await AppCredential.findAll();

        for (const cred of appCreds) {
            //
            // CLUSTERING
            //
            let puppetBot = cluster.fork();

            //
            // WORKER INCOMING MESSAGE HANDLER
            //
            puppetBot.on("message", async (inMsg) => {
                console.log(inMsg);
                const agent = inMsg.agent;
                const user = await agent.getUser();
                // [PROGRESS]
                // Send telegram notification
                const bot = new Telegram(process.env.TELEGRAM_BOT_TOKEN);
                bot.sendMessage(
                    user.telegramId,
                    `topup channel sudah dilakukan`,
                    {
                        parse_mode: "HTML",
                    }
                );
            });

            puppetBots.push(puppetBot);
        }
    })();
}

// ******************
// * CHILD PROCESS *
// * PUPETTER BOT *
// ******************
else {
    (async () => {
        //
        // CHILD MESSAGE HANDLER
        //
        process.on("message", async (inMsg) => {
            try {
                await topuping(inMsg.agent);
                process.send({
                    success: true,
                    message: "Topup success",
                    agent: inMsg.agent,
                });
            } catch (err) {
                if (err instanceof BotError) {
                    process.send({
                        success: false,
                        errorCode: err.errorCode,
                        errMessage: err.message,
                        agent: inMsg.agent,
                    });
                }
            }
        });

        //
        // INIT PUPPETEER
        //
        const browser = await puppeteer.launch({ headless: "new" });
        const page = await browser.newPage();
        await page.setViewport({ width: 1024, height: 768 });

        // *********
        // FUNCTIONS
        // *********

        //
        // PUPPET TOPUP-ING
        //
        const topuping = async (agent) => {
            // CHECKING IF THE PAGE IS NOT CHANGED
            let tFrame = page.frames()[1];
            if (!tFrame) {
                await login();
                await topuping();
                return;
            }

            // CHECKING THE PAGE HAS "CHANNEL TO CHANNEL" MENU
            let channelToChannelMenu = await tFrame.$(
                `a[href='/pretups/channelToChannelSearchAction.do?method=userSearch&moduleCode=CHNL2CHNL']`
            );

            if (!channelToChannelMenu) {
                await login();
                await topuping();
                return;
            }
            console.log(`puppet #${cluster.worker.id} sudah dashboard`);

            // CLICKING MENU "CHANNEL TO CHANNEL" AND WAIT FOR "CHANNEL TO CHANNEL" PAGE APPEAR
            await Promise.all([
                tFrame.waitForNavigation(),
                channelToChannelMenu.click(),
            ]);

            // [DEV] - SCREENSHOT CHANNEL TO CHANNEL PAGE
            //
            await page.screenshot({
                type: "jpeg",
                path: `screenshots/sc${process.pid}-channel-to-channel.jpeg`,
            });
        };

        //
        // PUPPET LOGIN
        //
        const login = async () => {
            //
            // NAVIGATE TO URL
            //
            await page.goto("https://cuan.tri.co.id/pretups/");

            // CHECK IF BOT HAS LANDED TO THE RIGHT PAGE
            let loginBtn = await page.$(
                `input[type=submit][name=submit1][value=Login]`
            );
            let loginIdInput = await page.$(`input#loginId`);
            let passwordInput = await page.$(`input#password`);

            // console.log(`login btn`, loginBtn ? true : false);
            // console.log(`loginIdInput`, loginIdInput ? true : false);
            // console.log(`passwordInput`, passwordInput ? true : false);

            if (!(loginBtn && loginIdInput && passwordInput)) {
                throw new BotError(
                    `puppet #${cluster.worker.id} - Webpage tidak sesuai`,
                    BOT_ERROR_CODES.webpageChanged
                );
            }

            // GET THE CREDENTIAL
            const appCred = await AppCredential.findOne({
                where: { workerId: cluster.worker.id },
            });
            if (!appCred) {
                throw new BotError(
                    `puppet #${cluster.worker.id} - Credential tidak ersedia`,
                    BOT_ERROR_CODES.dataNotAvailable
                );
            }

            // TYPE THE USERNAME
            await loginIdInput.type(appCred.username, { delay: 100 });

            // TYPE THE PASSWORD
            await passwordInput.type(appCred.password, { delay: 100 });

            // CLICK LOGIN AND WAIT FOR NEW PAGE
            await Promise.all([page.waitForNavigation(), loginBtn.click()]);

            // [DEV] - SCREENSHOT DASHBOARD
            //
            // await page.screenshot({
            //     type: "jpeg",
            //     path: `screenshots/puppet-${cluster.worker.id}-${process.pid}-setelah-login.jpeg`,
            // });

            // MAKE SURE NOT REDIRECT TO RELOGIN PAGE
            let reloginFrame = page.frames()[1];
            let reloginBtn = await reloginFrame.$(`input[name=relogin]`);
            let exitBtn = await reloginFrame.$(`input[name=exit]`);
            if (reloginBtn && exitBtn) {
                console.log(`puppet #${cluster.worker.id} masuk relogin page`);
                // [DEV] - SCREENSHOT RELOGIN PAGE
                //
                // await page.screenshot({
                //     type: "jpeg",
                //     path: `screenshots/puppet-${cluster.worker.id}-${process.pid}-relogin-page.jpeg`,
                // });
                await Promise.all([
                    exitBtn.click(),
                    reloginFrame.waitForNavigation(),
                ]);
                await login();
                return;
            }

            // MAKE SURE IT IS THE RIGHT DASHBOARD
            let tFrame = page.frames()[1];
            let tMenu = await tFrame.$(
                `a[href='/pretups/channelToChannelSearchAction.do?method=userSearch&moduleCode=CHNL2CHNL']`
            );
            if (!tMenu) {
                throw new BotError(
                    `puppet #${cluster.worker.id} Page telah berubah`,
                    BOT_ERROR_CODES.webpageChanged
                );
            }
        };

        try {
            if (cluster.worker.id === 1) await topuping();
            // await login();
        } catch (e) {
            if (e instanceof TimeoutError) {
                console.error(e.message);
            }
            if (e instanceof BotError) {
                console.error(e.message);
            }
        }
    })();
}

// *************
// API FUNCTIONS
// *************
export const topupChannel = async (agent) => {
    const appCred = await agent.getAppcredential();
    puppetBots[appCred.workerId].send({
        agent,
    });
};
