import "dotenv/config";
import cluster from "node:cluster";
import process from "node:process";

const resolvers = [];
let childFork = null;
// MASTER
if (cluster.isPrimary) {
    childFork = cluster.fork();
    childFork.on(`message`, async (inc) => {
        // cari resolver id
        let resolver = resolvers.find((arrval) => arrval.id === inc.resolverId);
        if (inc.success) {
            resolver.resolve(`${inc.resolverId} - berhasil`);
            return;
        }

        resolver.resolve(`${inc.resolverId} - gagal`);
    });
}
// CHILD
else {
    process.on(`message`, async (inc) => {
        const { resolverId, data } = inc;
        console.log(
            `worker menerima "message event"\nresolverID : ${resolverId}\ndata : ${data}`
        );
        // GENAP : SUCCESS
        if (resolverId % 2 === 0) {
            process.send({
                resolverId,
                success: true,
            });
            return;
        }

        // GANJIL : ERROR
        process.send({
            resolverId,
            success: false,
        });
    });
}

// API
export const apiFoo = async (pesan) => {
    if (cluster.isPrimary) {
        return new Promise((resolve, reject) => {
            const resolverId = Date.now();
            const data = `resolver #${resolverId} : "${pesan}"`;
            resolvers.push({
                id: resolverId,
                data,
                resolve,
            });
            childFork.send({
                resolverId,
                data,
            });
        });
    }

    console.log(cluster.worker.id);
};
