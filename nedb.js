import Nedb from "@seald-io/nedb";

export const db = new Nedb({
    autoload: true,
    filename: `${process.env.NEDB_FILENAME}`,
});
