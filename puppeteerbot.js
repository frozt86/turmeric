import "dotenv/config";
import cluster from "node:cluster";
import process from "node:process";
import puppeteer from "puppeteer";

import { Agent, AppCredential } from "./sequelize";

const puppetBots = [];

export const tembakPulsa = async (agent, nominal) => {
    puppetBots[0].send(`wuhui!!!`);
};

(async () => {
    //
    // GETTING BROWBOT CREDENTIALS
    //
    const BotCreds = [
        {
            id: 1,
            username: `08988087781`,
            password: `Apd@prb2`,
        },
        {
            id: 2,
            username: `0895371051997`,
            password: `Apd@jbr3`,
        },
    ];

    if (cluster.isPrimary) {
        //
        //  * MAIN PROCESS *
        //  * HTTP SERVER *
        //  * EXPRESS APP *
        //

        //
        // INITIALIZE EXPRESS SERVER
        //
        const app = express();
        app.use(bodyParser.json());

        //
        // LOCALHOST ONLY CHECKER MIDDLEWARE
        //
        app.use("/", (req, res, next) => {
            next();
        });

        //
        // GraphQL Schema
        //
        const schema = buildSchema(await readFile("browbot/graphql/schema.graphql", { encoding: "utf8" }));

        app.use(
            "/",
            graphqlHTTP({
                schema,
                rootValue: root,
                graphiql: true,
            }),
        );

        app.listen(parseInt(process.env.BROWSER_BOT_PORT), () => {
            console.log(`BROWBOT SERVER started\nListening on port ${process.env.BROWSER_BOT_PORT}`);
        });

        //
        // CLustering the Browser Bot
        //

        for (const botCred of BotCreds) {
            let puppetBot = cluster.fork();

            //
            // Message handler from WORKER
            //
            puppetBot.on("message", (inMsg) => {
                console.log(`dapat message dari worker #${puppetBot.id} "${inMsg}"`);
                puppetBot.send(`wuhui!!!`);
            });
            puppetBots.push(puppetBot);
        }
    } else {
        // ******************
        // * CHILD PROCESS *
        // * PUPETTER BOT *
        // ******************

        // *********
        // FUNCTIONS
        // *********
        //
        // LOGIN or RELOGIN
        //
        const login = async () => {
            //
            // Check current login
            //
            // [TASK] : CHECK CURRENT LOGIN

            await page.screenshot({
                type: "jpeg",
                path: `screenshots/sc${process.pid}-awal-login.jpeg`,
            });

            // Get the browser bot credential
            const cred = BotCreds.find((val) => val.id == cluster.worker.id);

            // Type Username
            await page.type(`input#loginId`, cred.username, {
                delay: 100,
            });

            // Type Username
            await page.type(`input#password`, cred.password, {
                delay: 100,
            });

            await page.screenshot({
                type: "jpeg",
                path: `screenshots/sc${process.pid}-sebelum-login.jpeg`,
            });

            // click login button
            await page.click(`input[type=submit][name=submit1]`);

            // await for navigation
            await page.waitForNavigation({
                timeout: 0,
            });

            await page.screenshot({
                type: "jpeg",
                path: `screenshots/sc${process.pid}-sesudah-login.jpeg`,
            });

            return true;
        };

        //
        // INIT PUPPETEER
        //
        const browser = await puppeteer.launch({ headless: "new" });
        const page = await browser.newPage();
        await page.setViewport({ width: 1024, height: 768 });

        // Navigate the page to a URL
        await page.goto("https://cuan.tri.co.id/pretups/");

        // Login into system
        await loginIntoSystem();

        //
        // Message handler from PRIME PROCESS
        //
        process.on("message", (inMsg) => {
            console.log(`worker #${cluster.worker.id} incoming message "${inMsg}"`);
        });

        async function loginIntoSystem() {}
    }
})();
