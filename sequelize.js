import { DataTypes, Model, Sequelize } from "sequelize";

export const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: `${process.env.SQLITE_FILENAME}`,
    logging: false,
});

//
// USER MODEL
//
export const User = sequelize.define(
    "user",
    {
        nama: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        telegramId: {
            type: DataTypes.INTEGER,
        },
        telegramUsername: {
            type: DataTypes.STRING,
        },
    },
    {
        paranoid: true,
    }
);

//
// ADMIN MODEL
//
export const Admin = sequelize.define(
    "admin",
    {
        nama: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        isSuperadmin: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
    },
    {
        paranoid: true,
    }
);

//
// AGENT MODEL
//
export const Agent = sequelize.define(
    "agent",
    {
        nama: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        nomorChannel: {
            type: DataTypes.STRING(15),
            allowNull: false,
        },
        keterangan: {
            type: DataTypes.TEXT,
        },
    },
    {
        paranoid: true,
    }
);

//
// CREDENTIAL MODEL
//
export const AppCredential = sequelize.define(
    "appcredential",
    {
        workerId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        username: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING(511),
            allowNull: false,
        },
        pin: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    },
    {
        paranoid: true,
    }
);

//
// AGENT ACTION LOG MODEL
//
export const AgentActionLog = sequelize.define(
    "agentactionlog",
    {
        log: {
            type: DataTypes.TEXT,
        },
    },
    {
        paranoid: true,
    }
);

//
// SYSTEM VAR MODEL
//
export const UserBotSession = sequelize.define(
    "sysvar",
    {
        session: {
            type: DataTypes.JSON,
        },
    },
    {
        paranoid: false,
        timestamps: false,
    }
);

//
// SYSTEM VAR MODEL
//
export const SysVar = sequelize.define(
    "sysvar",
    {
        var: {
            type: DataTypes.STRING(63),
        },
        value: {
            type: DataTypes.JSON,
        },
    },
    {
        paranoid: false,
        timestamps: false,
    }
);

//
// ASSOSIATION
// RELATIONSHIP
//
User.hasOne(Admin);
User.hasOne(Agent);
User.hasOne(UserBotSession);

Admin.belongsTo(User);

Agent.belongsTo(User);
Agent.belongsTo(AppCredential);
Agent.hasMany(AgentActionLog);

AppCredential.hasMany(Agent);

AgentActionLog.belongsTo(Agent);

UserBotSession.belongsTo(User);
