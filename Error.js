// ************
// PUPPET ERROR
// ************
export class PuppetError extends Error {
    constructor(message, errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
}

export const PUPPET_ERROR_CODES = {
    loginFailed: 5001,
    procedureStepFailed: 5002,
    webpageChanged: 5003,
    navigationFailed: 5004,
    dataNotAvailable: 5005,
};
